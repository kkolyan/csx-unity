namespace Kk.CsxUnity
{
    public static class NullableIntMethods
    {
        public static int? GetValueIfPresent(in this NullableInt value)
        {
            if (value.present)
            {
                return value.value;
            }

            return null;
        }
    }
}