using UnityEngine;

namespace Kk.CsxUnity
{
    public static class SafeTime
    {
        // slow down game time if fps drops below 30 to avoid too large game steps
        public static float DeltaTime => Mathf.Min(Time.deltaTime, 1f / 30f);
    }
}