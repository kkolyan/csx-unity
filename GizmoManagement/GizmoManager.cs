using System.Collections.Generic;
using UnityEngine;

namespace Kk.CsxUnity.GizmoManagement
{
    [ExecuteAlways]
    public class GizmoManager: MonoBehaviour
    {
        private IList<GizmoLine> _lines;
        private IList<GizmoSphere> _spheres;

        private void OnEnable()
        {
            _lines = new List<GizmoLine>();
            _spheres = new List<GizmoSphere>();
        }

        public void AddEdge(Vector3 begin, Vector3 end, Color color)
        {
            _lines.Add(new GizmoLine(begin, end, color));
        }

        public void AddSphere(Vector3 center, float radius, Color color, bool wire)
        {
            _spheres.Add(new GizmoSphere(center, radius, wire: wire, color: color));
        }

        private void OnDrawGizmosSelected()
        {
            foreach (var line in _lines)
            {
                Gizmos.color = line.Color;
                Gizmos.DrawLine(line.Begin, line.End);
            }

            foreach (var sphere in _spheres)
            {
                Gizmos.color = sphere.Color;
                if (sphere.Wire)
                {
                    Gizmos.DrawWireSphere(sphere.Center, sphere.Radius);
                }
                else
                {
                    Gizmos.DrawSphere(sphere.Center, sphere.Radius);
                }
            }
        }

        public void ResetGizmos()
        {
            _lines.Clear();
            _spheres.Clear();
        }
    }
}