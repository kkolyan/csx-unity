using UnityEngine;

namespace Kk.CsxUnity.GizmoManagement
{
    public struct GizmoLine
    {
        public Vector3 Begin;
        public Vector3 End;
        public Color Color;


        public GizmoLine(Vector3 begin, Vector3 end, Color color)
        {
            Begin = begin;
            End = end;
            Color = color;
        }
    }
}