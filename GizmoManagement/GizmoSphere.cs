using UnityEngine;

namespace Kk.CsxUnity.GizmoManagement
{
    public struct GizmoSphere
    {
        public Vector3 Center;
        public float Radius;
        public bool Wire;
        public Color Color;

        public GizmoSphere(Vector3 center, float radius, bool wire, Color color)
        {
            Center = center;
            Radius = radius;
            Wire = wire;
            Color = color;
        }
    }
}