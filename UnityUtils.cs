using System.Runtime.CompilerServices;
using UnityEngine;

namespace Kk.CsxUnity
{
    public static class UnityUtils
    {
        public static T IfEnabled<T>(this T t) where T : Behaviour
        {
            if (t.enabled)
            {
                return t;
            }

            return null;
        }

        public static float CalculateRandomInRange(this Vector2 range)
        {
            return Random.Range(range.x, range.y);
        }

        public static Vector2 InverseRange(this Vector2 range)
        {
            return new Vector2(range.y, range.x);
        }

        public static float ClampToRange(this Vector2 range, float value)
        {
            return Mathf.Clamp(value, range.x, range.y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 WithY(this Vector3 vector, float y)
        {
            return new Vector3(vector.x, y, vector.z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Color WithAlpha(this Color color, float a)
        {
            return new Color(color.r, color.g, color.b, a);
        }

        public static bool TestLayerAgainstMask(this int layer, int layerMask)
        {
            return layerMask == (layerMask | (1 << layer));
        }

        public static Vector2 GetXY(this Vector3 v)
        {
            return new Vector2(v.x, v.y);
        }

        public static Vector2Int GetXY(this Vector3Int v)
        {
            return new Vector2Int(v.x, v.y);
        }

        public static Vector2 Mult(this Vector2 v, Vector2 other)
        {
            return new Vector2(v.x * other.x, v.y * other.y);
        }

        public static bool MaskContains(this LayerMask mask, int layer)
        {
            return mask == (mask | (1 << layer));
        }

        // to safely use elvis operator with Unity types
        public static T SafeNull<T>(this T obj) where T : Object
        {
            if (obj == null)
            {
                return null;
            }

            return obj;
        }
    }
}