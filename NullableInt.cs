using System;

namespace Kk.CsxUnity
{
    [Serializable]
    public struct NullableInt
    {
        public bool present;
        public int value;
    }
}