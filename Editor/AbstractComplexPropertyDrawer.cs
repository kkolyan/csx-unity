using System;
using System.Collections.Generic;
using Kk.CsxCore;
using UnityEditor;
using UnityEngine;

namespace Kk.CsxUnity.Editor
{
    public abstract class AbstractComplexPropertyDrawer : PropertyDrawer
    {
        private readonly List<Field> _fields = new List<Field>();
        private readonly List<ActionButton> _actions = new List<ActionButton>();
        private const int Interval = 0;

        private struct Field
        {
            public string Name;
            public string Title;
            public float LabelRatio;
        }

        protected void AddField(string name, string title, float labelRatio)
        {
            _fields.Add(new Field
            {
                Name = name,
                Title = title,
                LabelRatio = labelRatio,
            });
        }

        protected void AddActionButton(string caption, Action<SerializedProperty> action)
        {
            _actions.Add(new ActionButton
            {
                Action = action,
                Caption = caption,
            });
        }

        private struct ActionButton
        {
            public string Caption;
            public Action<SerializedProperty> Action;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            var columnsCount = _fields.Count + _actions.Count;
            var fieldWidth = (position.width - Interval * (columnsCount - 1)) / columnsCount;

            var x = position.x;

            var prevLabelWidth = EditorGUIUtility.labelWidth;
            
            foreach (var field in _fields)
            {
                Rect totalPosition = new Rect(x, position.y, fieldWidth, position.height);
                EditorGUI.HandlePrefixLabel(totalPosition, totalPosition, new GUIContent(field.Title));
                EditorGUI.PropertyField(
                    totalPosition,
                    property.FindPropertyRelative(field.Name),
                    GUIContent. none
                );
                x += fieldWidth + Interval;
            }

            foreach (var field in _actions)
            {
                Rect totalPosition = new Rect(x, position.y, fieldWidth, position.height);
                if (GUI.Button(totalPosition, field.Caption))
                {
                    field.Action(property);
                }
                x += fieldWidth + Interval;
            }

            EditorGUI.EndProperty();

            EditorGUIUtility.labelWidth = prevLabelWidth;
        }
    }
}