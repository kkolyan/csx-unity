using Kk.CsxUnity.GizmoManagement;
using UnityEditor;
using UnityEngine;

namespace Kk.CsxUnity.Editor
{
    [CustomEditor(typeof(GizmoManager))]
    public class GizmoManagerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            // base.OnInspectorGUI();
            var gizmoManager = (GizmoManager) target;
            if (GUILayout.Button("Reset"))
            {
                gizmoManager.ResetGizmos();
            }
        }
    }
}