using UnityEditor;
using UnityEngine;

namespace Kk.CsxUnity.Editor
{
    [CustomPropertyDrawer(typeof(NullableInt))]
    public class NullableIntPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(
            Rect position,
            SerializedProperty property,
            GUIContent label
        )
        {
            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            var present = property.FindPropertyRelative(nameof(NullableInt.present));

            const float toggleWidth = 20;

            present.boolValue = EditorGUI.ToggleLeft(
                new Rect(-60 + position.x, position.y, 60 + toggleWidth, position.height),
                "",
                present.boolValue
            );

            const float toggleMargin = -20;

            using (new EditorGUI.DisabledScope(!present.boolValue))
            {
                // EditorGUIUtility.labelWidth = 10;
                EditorGUI.PropertyField(
                    new Rect(position.x + toggleWidth + toggleMargin, position.y, position.width - (toggleWidth + toggleMargin), position.height),
                    property.FindPropertyRelative(nameof(NullableInt.value)),
                    GUIContent.none
                );
            }


            EditorGUI.EndProperty();
        }
    }
}