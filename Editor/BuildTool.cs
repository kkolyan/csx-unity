using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

namespace Kk.CsxUnity.Editor
{
    public static class BuildTool
    {
        [MenuItem("My Tools/Build/Windows")]
        public static void BuildGame()
        {
            BuildGameInternal(BuildTarget.StandaloneWindows);
        }
    
        [MenuItem("My Tools/Build/Windows64")]
        public static void BuildGame64()
        {
            BuildGameInternal(BuildTarget.StandaloneWindows64);
        }

        private static void BuildGameInternal(BuildTarget target)
        {
            var path = $"{Application.dataPath}/../target/{Application.productName}";

            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            // Build player.
            BuildPipeline.BuildPlayer(
                levels: new[] {SceneManager.GetActiveScene().path},
                locationPathName: $"{path}/{Application.productName}.exe",
                target: target,
                options: BuildOptions.None
            );

            // look to FileUtil for more actions

            Debug.Log("Build complete. directory " + path);
            Process.Start(path);
        }
    }
}